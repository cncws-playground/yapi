# yapi docker 镜像

## 构建镜像

```sh
docker build -t yapi:1.10.2 .
```

## 初始化

```sh
docker run -ti --rm -v /path/to/config.json:/yapi/config.json yapi:1.10.2 npm run install-server
```

## 启动服务

```sh
docker run -d -v /path/to/config.json:/yapi/config.json -p 3000:3000 yapi:1.10.2
```
