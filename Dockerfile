FROM node:14 AS builder
WORKDIR /yapi/vendors
RUN git clone --depth=1 --branch v1.12.0 --single-branch https://gitee.com/mirrors/YApi.git /yapi/vendors \
    && rm package-lock.json \
    && npm install --production --registry https://registry.npm.taobao.org

FROM node:14-alpine
COPY --from=builder /yapi /yapi
CMD node /yapi/vendors/server/app.js
